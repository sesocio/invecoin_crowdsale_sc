const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const { expectEvent, BN, constants } = require('openzeppelin-test-helpers');

const FixedPointMath = artifacts.require('FixedPointMath');
const InveCoin = artifacts.require('InveCoin');
const InveCrowdsale = artifacts.require('InveCrowdsale');
const MockUSDETHFeeder = artifacts.require('MockUSDETHFeeder');

const {
  advanceTime,
  concatPromises,
  deployFinishedCrowdsaleContract,
  deployOpenCrowdsale,
  getCurrentTime,
} = require('./utils');

const { expect } = chai;

const { ZERO_ADDRESS } = constants;

const monthToSeconds = 30 * 60 * 60 * 24;
const companyTokensPercentage = 48;
const companyMonthsToEndVesting = 36;
const companyMontshToStartVesting = 18;

const conversionSecurityWeis = new BN(20); // Necesary because of the back and forth conversion
chai.use(chaiAsPromised);
chai.should();

contract('InveCoin', accounts => {
  const owner = accounts[0];
  const anUser = accounts[1];
  const anotherUser = accounts[2];
  const thirdUser = accounts[3];

  describe('Token', () => {
    describe('GIVEN the tokens is already in its pure ERC20 state', () => {
      let inveCoin;
      let inveCrowdsale;

      const openingTimeFromNow = 10;
      const crowdsaleDuration = 10000;
      const fundingCapInUsd = new BN('100000');
      const raisedPresaleInUsd = new BN('0');

      const addressesToWhitelist = [owner, anUser, anotherUser];
      const anUserInitialBalance = 100;
      const ownerInitialBalance = 1000;
      const companyAddress = thirdUser;

      beforeEach(async () => {
        ({ inveCoin, inveCrowdsale } = await deployOpenCrowdsale(
          owner,
          openingTimeFromNow,
          crowdsaleDuration,
          fundingCapInUsd,
          raisedPresaleInUsd,
          addressesToWhitelist,
        ));

        await inveCoin.mint(anUser, anUserInitialBalance, { from: owner });
        await inveCoin.mint(owner, ownerInitialBalance, { from: owner });

        await advanceTime(crowdsaleDuration + 1); // Make the crowdsale able to finish
        await inveCrowdsale.finish(companyAddress, { from: owner });
      });

      describe('WHEN a user without enough balance wants to transfer tokens', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin.transfer(anotherUser, anUserInitialBalance + 1, { from: anUser }).should.be
            .rejected;
        });
      });

      describe('WHEN a user without enough allowance wants to transfer tokens', () => {
        it('THEN the transaction fails', async () => {
          const allowedAmount = 40;
          await inveCoin.approve(anUser, allowedAmount, { from: owner });
          await inveCoin.transferFrom(owner, anotherUser, allowedAmount + 1, { from: anUser })
            .should.be.rejected;
        });
      });

      describe('WHEN a user with balance wants to transfer tokens', () => {
        let logs;
        const transferedAmount = 20;
        beforeEach(async () => {
          ({ logs } = await inveCoin.transfer(anotherUser, transferedAmount, { from: anUser }));
        });
        it("THEN the sender's balance decreases", async () => {
          expect(await inveCoin.balanceOf(anUser)).to.be.bignumber.equal(
            new BN(anUserInitialBalance - transferedAmount),
          );
        });

        it("THEN the receiver's balance increases", async () => {
          expect(await inveCoin.balanceOf(anotherUser)).to.be.bignumber.equal(
            new BN(transferedAmount),
          );
        });

        it('THEN the logs is emitted', async () => {
          expectEvent.inLogs(logs, 'Transfer', {
            from: anUser,
            value: new BN(transferedAmount),
            to: anotherUser,
          });
        });
      });
      describe('WHEN a user with balance wants to use an allowance', () => {
        let logs;
        const allowedAmount = 40;
        const transferedAmount = allowedAmount;
        beforeEach(async () => {
          await inveCoin.approve(anUser, allowedAmount, { from: owner });
          ({ logs } = await inveCoin.transferFrom(owner, anotherUser, transferedAmount, {
            from: anUser,
          }));
        });

        it("THEN the allower's balance decreases", async () => {
          expect(await inveCoin.balanceOf(owner)).to.be.bignumber.equal(
            new BN(ownerInitialBalance - transferedAmount),
          );
        });

        it("THEN the receiver's balance increases", async () => {
          expect(await inveCoin.balanceOf(anotherUser)).to.be.bignumber.equal(
            new BN(transferedAmount),
          );
        });

        it("THEN the allowance 's balance decreases", async () => {
          expect(await inveCoin.allowance(anotherUser, owner)).to.be.bignumber.equal(
            new BN(allowedAmount - transferedAmount),
          );
        });

        it('THEN the logs is emitted', async () => {
          expectEvent.inLogs(logs, 'Transfer', {
            from: owner,
            value: new BN(transferedAmount),
            to: anotherUser,
          });
        });
      });
    });
  });
  describe('Token-Crowdsale integration', () => {
    describe('GIVEN a token has no crowdsale', () => {
      let inveCoin;
      let inveCrowdsale;
      beforeEach(async () => {
        inveCoin = await InveCoin.new({ from: owner });
        const usdEthFeeder = await MockUSDETHFeeder.new({ from: owner });

        const openingTime = (await getCurrentTime()) + 100;
        const closingTime = openingTime + 100;

        const fixedPointMath = await FixedPointMath.new();

        await InveCrowdsale.link('FixedPointMath', fixedPointMath.address);
        inveCrowdsale = await InveCrowdsale.new(
          '10000',
          '100',
          usdEthFeeder.address,
          owner,
          inveCoin.address,
          openingTime,
          closingTime,
          [],
          [],
          [],
          [],
          [],
          48,
          { from: owner },
        );
      });

      describe('WHEN a non-owner user tries to set the crowdsale', () => {
        it('THEN the tx fails', async () => {
          await inveCoin.setCrowdsale(inveCrowdsale.address, { from: anUser }).should.be.rejected;
        });
      });

      describe('WHEN the owner tries to set the crowdsale twice', () => {
        it('THEN the tx fails', async () => {
          await inveCoin.setCrowdsale(inveCrowdsale.address, { from: owner });
          await inveCoin
            .setCrowdsale(inveCrowdsale.address, { from: owner })
            .should.be.rejectedWith('Crowdsale already set');
        });
      });

      describe('WHEN the owner tries to set the crowdsale as address 0', () => {
        it('THEN the tx fails', async () => {
          await inveCoin
            .setCrowdsale(ZERO_ADDRESS, { from: owner })
            .should.be.rejectedWith('Invalid crowdsale');
        });
      });

      describe('WHEN the owner tries to set the crowdsale as address 0', () => {
        it('THEN the tx fails', async () => {
          await inveCoin
            .setCrowdsale(ZERO_ADDRESS, { from: owner })
            .should.be.rejectedWith('Invalid crowdsale');
        });
      });
    });

    describe('GIVEN the token has an open crowdsale', () => {
      let inveCrowdsale;
      let inveCoin;

      const openingTimeFromNow = 10;
      const crowdsaleDuration = 10000;
      const fundingCapInUsd = new BN('100000');
      const raisedPresaleInUsd = new BN('0');

      const addressesToWhitelist = [owner, anUser, anotherUser];
      beforeEach(async () => {
        ({ inveCrowdsale, inveCoin } = await deployOpenCrowdsale(
          owner,
          openingTimeFromNow,
          crowdsaleDuration,
          fundingCapInUsd,
          raisedPresaleInUsd,
          addressesToWhitelist,
        ));
      });

      describe('WHEN a user tries to finish the crowdsale from the token ', () => {
        it('THEN the tx fails because the crowdsale should notify the token', async () => {
          await inveCoin.finishCrowdsale().should.be.rejectedWith('Sender is not set crowdsale');
        });
      });
      describe('WHEN a user tries to transfer its tokens', () => {
        it('THEN the tx fails because the token is paused', async () => {
          const priceOfFirstTokenInWei = await inveCrowdsale.priceOfNextTokenInWei();
          await inveCrowdsale.buyTokens(owner, {
            from: owner,
            value: priceOfFirstTokenInWei.add(conversionSecurityWeis),
          });

          const balance = await inveCoin.balanceOf(owner);
          expect(balance).to.be.bignumber.gt(new BN(0));
          await inveCoin.transfer(anUser, balance, { from: owner }).should.be.rejected;
        });
      });

      describe('WHEN the crowdsale finishes', () => {
        it("THEN the company token's are distributed", async () => {
          const companyAddress = thirdUser;
          const priceOfFirstTokenInWei = await inveCrowdsale.priceOfNextTokenInWei();
          await inveCrowdsale.buyTokens(owner, {
            from: owner,
            value: priceOfFirstTokenInWei.mul(new BN(1000)),
          });
          const tokensBought = await inveCoin.balanceOf(owner);

          const tokensDistributed = tokensBought
            .mul(new BN(companyTokensPercentage))
            .div(new BN(100 - companyTokensPercentage));
          await advanceTime(crowdsaleDuration + 1);
          const { logs } = await inveCrowdsale.finish(companyAddress, { from: owner });
          expectEvent.inLogs(logs, 'CrowdsaleFinished', {
            companyAddress,
            tokensDistributed,
          });
        });
      });

      describe('WHEN the crowdsale finishes', () => {
        it('THEN the token is unpaused', async () => {
          const priceOfFirstTokenInWei = await inveCrowdsale.priceOfNextTokenInWei();
          await inveCrowdsale.buyTokens(owner, {
            from: owner,
            value: priceOfFirstTokenInWei.add(conversionSecurityWeis),
          });
          const balance = await inveCoin.balanceOf(owner);
          expect(balance).to.be.bignumber.gt(new BN(0));
          await advanceTime(crowdsaleDuration + 1);

          await inveCrowdsale.finish(owner, { from: owner });
          await inveCoin.transfer(anUser, balance, { from: owner });
          expect(await inveCoin.balanceOf(anUser)).to.be.bignumber.equal(balance);
        });
      });
    });
    describe('GIVEN a contract has a finished crowdsale', () => {
      let inveCoin;
      beforeEach(async () => {
        ({ inveCoin } = await deployFinishedCrowdsaleContract(owner));
      });

      describe('WHEN a minter tries to mint', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin
            .mint(owner, 2, { from: owner })
            .should.be.rejectedWith('Crowdsale has finished already');
        });
      });

      describe('WHEN a minter tries to add more vested tokens', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin
            .addVesting(owner, 2, 1, 2, { from: owner })
            .should.be.rejectedWith('Vesting already started');
        });
      });
    });
  });

  describe('Vesting', () => {
    describe('GIVEN the token has an open crowdsale', () => {
      let inveCoin;

      const openingTimeFromNow = 10;
      const crowdsaleDuration = 10000;
      const fundingCapInUsd = new BN('100000');
      const raisedPresaleInUsd = new BN('0');

      const addressesToWhitelist = [owner, anUser, anotherUser];
      beforeEach(async () => {
        ({ inveCoin } = await deployOpenCrowdsale(
          owner,
          openingTimeFromNow,
          crowdsaleDuration,
          fundingCapInUsd,
          raisedPresaleInUsd,
          addressesToWhitelist,
        ));
      });
      describe('WHEN a non-minter tries to add vested tokens', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin.addVesting(anUser, 100, 3, 6, { from: anUser }).should.be.rejected;
        });
      });

      describe('WHEN a minter tries to add vesteable tokens more than once', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin.addVesting(anUser, 100, 3, 6, { from: owner });
          await inveCoin
            .addVesting(anUser, 100, 3, 6, { from: owner })
            .should.be.rejectedWith('Address in use');
        });
      });

      describe('WHEN a minter tries to add vesteable tokens and the length is inconsistent', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin
            .addVestingBatch([anUser], [100], [3, 2], [6], { from: owner })
            .should.be.rejectedWith('Arrays length mismatch');
        });
      });

      describe('WHEN a minter tries to add vesting tokens and the start and end month are equal', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin
            .addVesting(anUser, 100, 3, 3, { from: owner })
            .should.be.rejectedWith('No time to vest');
        });
      });

      describe('WHEN a minter tries to add vesting tokens and the start and end month are equal', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin
            .addVesting(anUser, 0, 3, 6, { from: owner })
            .should.be.rejectedWith('No tokens to vest');
        });
      });

      describe('WHEN a user tries to take tokens before the crowdsale is able to finish', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin.addVesting(anUser, 100, 3, 6, { from: owner });
          await inveCoin.claim({ from: anUser }).should.be.rejectedWith('Didnt start vesting yet');
        });
      });

      describe('WHEN a user tries to take tokens before the crowdsale finishes explicitly', () => {
        it('THEN the transaction fails', async () => {
          await inveCoin.addVesting(anUser, 100, 3, 6, { from: owner });
          await advanceTime(crowdsaleDuration);
          await inveCoin.claim({ from: anUser }).should.be.rejectedWith('Didnt start vesting yet');
        });
      });
    });

    describe('GIVEN a contract has a finished crowdsale', () => {
      describe('AND a user has 10 vested tokens charged as private', () => {
        let inveCoin;
        beforeEach(async () => {
          ({ inveCoin } = await deployFinishedCrowdsaleContract(owner, [anUser], [10], [6], [12]));
        });

        describe('WHEN he tries to claim them progressively', () => {
          it('THEN he can only claim the amount he is supposed to do every time and the final amount is 10', async () => {
            let logs;
            const expectedAmountsToTake = [1, 2, 2, 1, 2, 2];

            const checkNextStep = async amountExpected => {
              await advanceTime(monthToSeconds * 1);
              ({ logs } = await inveCoin.claim({ from: anUser }));
              expectEvent.inLogs(logs, 'TokensClaimed', {
                amount: new BN(amountExpected),
                user: anUser,
              });
              expectEvent.inLogs(logs, 'Transfer', {
                value: new BN(amountExpected),
                to: anUser,
                from: ZERO_ADDRESS,
              });
            };
            // Only five months because we want to add the last month later
            await advanceTime(monthToSeconds * 6);

            await concatPromises(expectedAmountsToTake, checkNextStep);
          });
        });
      });

      describe('AND a user has 100 vested tokens charged as pre-sales', () => {
        let inveCrowdsale;
        let inveCoin;

        const openingTimeFromNow = 100;
        const crowdsaleDuration = 10000;
        const fundingCapInUsd = new BN('100000');
        const raisedPresaleInUsd = new BN('0');
        const companyAddress = thirdUser;

        const addressesToWhitelist = [owner, anUser, anotherUser];
        beforeEach(async () => {
          ({ inveCrowdsale, inveCoin } = await deployOpenCrowdsale(
            owner,
            openingTimeFromNow,
            crowdsaleDuration,
            fundingCapInUsd,
            raisedPresaleInUsd,
            addressesToWhitelist,
          ));
          await inveCoin.addVestingBatch([anUser], [100], [3], [6], { from: owner });
          await advanceTime(crowdsaleDuration + 1); // Make the crowdsale able to finish
          await inveCrowdsale.finish(companyAddress, { from: owner });
        });

        describe('WHEN he tries to claim them immediately', () => {
          it('THEN the transaction fails', async () => {
            await inveCoin
              .claim({ from: anUser })
              .should.be.rejectedWith('Has not reached vesting start date');
          });
        });

        describe('WHEN he claims them at the end of the 4th month', () => {
          it('THEN the users gets a third of his tokens', async () => {
            await advanceTime(monthToSeconds * 4);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(33), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(33),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(33);
          });

          it('THEN the users vestingBalance decreases a third of his tokens', async () => {
            const oldVestingBalance = await inveCoin.vestingBalanceOf(anUser);
            await advanceTime(monthToSeconds * 4);
            await inveCoin.claim({ from: anUser });
            await expect(await inveCoin.vestingBalanceOf(anUser)).to.be.bignumber.equal(
              oldVestingBalance.sub(new BN(33)),
            );
          });

          it('THEN the totalSupply doesnt change', async () => {
            await advanceTime(monthToSeconds * 4);
            const oldTotalSupply = await inveCoin.totalSupply();
            await inveCoin.claim({ from: anUser });
            expect(await inveCoin.totalSupply()).to.be.bignumber.equal(oldTotalSupply);
          });

          it('THEN the vestingSupply decreases in the amount received by the user tokens', async () => {
            await advanceTime(monthToSeconds * 4);
            const oldBalance = await inveCoin.balanceOf(anUser);
            const oldVestingSupply = await inveCoin.vestingSupply();
            await inveCoin.claim({ from: anUser });
            const newBalance = await inveCoin.balanceOf(anUser);
            const claimedAmount = newBalance.sub(oldBalance);
            expect(await inveCoin.vestingSupply()).to.be.bignumber.equal(
              oldVestingSupply.sub(claimedAmount),
            );
          });

          it('THEN the circulatingSupply increases in the amount received by the user tokens', async () => {
            await advanceTime(monthToSeconds * 4);
            const oldBalance = await inveCoin.balanceOf(anUser);
            const oldCirculatingSupply = await inveCoin.circulatingSupply();
            await inveCoin.claim({ from: anUser });
            const newBalance = await inveCoin.balanceOf(anUser);
            const claimedAmount = newBalance.sub(oldBalance);
            expect(await inveCoin.circulatingSupply()).to.be.bignumber.equal(
              oldCirculatingSupply.add(claimedAmount),
            );
          });
        });

        describe('WHEN he tries to claim them at the middle of the 5th month, i.e. at the middle of the vesting time', () => {
          it('THEN the transaction doesnt fail AND the user can claim a half of his tokens', async () => {
            await advanceTime(monthToSeconds * 4.5);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(50), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(50),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(50);
          });
        });

        describe('WHEN he tries to claim them at the middle at the 73%(arbitrary number) the vesting time', () => {
          it('THEN the transaction doesnt fail AND the user can claim a half of his tokens', async () => {
            const vestingDurationInMonths = 6 - 3;
            const monthsToStartVesting = 3;
            // Advance to the vesting starting point
            await advanceTime(monthToSeconds * monthsToStartVesting);
            // Advance 73% of the vesting duration
            await advanceTime((monthToSeconds * vestingDurationInMonths * 73) / 100);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(73), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(73),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(73);
          });
        });

        describe('WHEN he tries to claim them immediately', () => {
          describe('AND later he tries to claim them at the end of the 4th month', () => {
            it('THEN the transaction doesnt fail AND the user can claim his tokens', async () => {
              await inveCoin
                .claim({ from: anUser })
                .should.be.rejectedWith('Has not reached vesting start date');
              await advanceTime(monthToSeconds * 4);
              const { logs } = await inveCoin.claim({ from: anUser });
              expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(33), user: anUser });
              expectEvent.inLogs(logs, 'Transfer', {
                value: new BN(33),
                to: anUser,
                from: ZERO_ADDRESS,
              });
              await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(33);
            });
          });
        });

        describe('WHEN he tries to claim them on the 10th month', async () => {
          it('THEN he will only have 10 tokens', async () => {
            await advanceTime(monthToSeconds * 9);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(100), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(100),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(100);
          });
        });

        describe('WHEN he claims them ', async () => {
          it('THEN the totalSupply doesnt change', async () => {
            await advanceTime(monthToSeconds * 6);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(100), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(100),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(100);
          });
        });

        describe('WHEN he tries to claim twice them on the 7th month', async () => {
          it('THEN he will only have 10 tokens', async () => {
            await advanceTime(monthToSeconds * 6);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(100), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(100),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await inveCoin
              .claim({ from: anUser })
              .should.be.rejectedWith('The user has claimed all the tokens he can claim for now');
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(100);
          });
        });

        describe('WHEN he tries to claim at the end of the 4th month twice', () => {
          it('THEN the second transaction fails and the first one succeds', async () => {
            await advanceTime(monthToSeconds * 4);
            const { logs } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(33), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(33),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await inveCoin
              .claim({ from: anUser })
              .should.be.rejectedWith('The user has claimed all the tokens he can claim for now');
            await expect((await inveCoin.balanceOf(anUser)).toNumber()).to.be.equal(33);
          });
        });

        describe('AND another user has not', () => {
          it('THEN the second user can not claim any', async () => {
            await inveCoin
              .claim({ from: anotherUser })
              .should.be.rejectedWith('User has not vesteable tokens');
          });
        });

        describe('WHEN he tries to claim them progressively', () => {
          it('THEN he can only claim the amount he is supposed to do(rounded down) every time', async () => {
            let logs;
            await advanceTime(monthToSeconds * 4);
            ({ logs } = await inveCoin.claim({ from: anUser }));
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(33), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(33),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await advanceTime(monthToSeconds * 1);
            ({ logs } = await inveCoin.claim({ from: anUser }));
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(33), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(33),
              to: anUser,
              from: ZERO_ADDRESS,
            });
            await advanceTime(monthToSeconds * 1);
            ({ logs } = await inveCoin.claim({ from: anUser }));
            expectEvent.inLogs(logs, 'TokensClaimed', { amount: new BN(34), user: anUser });
            expectEvent.inLogs(logs, 'Transfer', {
              value: new BN(34),
              to: anUser,
              from: ZERO_ADDRESS,
            });
          });
        });

        describe('WHEN a company member tries to claim his tokens immediately', () => {
          it('THEN the transaction fails', async () => {
            await inveCoin
              .claim({ from: anUser })
              .should.be.rejectedWith('Has not reached vesting start date');
          });
        });

        describe(`WHEN a company member tries to claim his tokens at the end of the ${companyMontshToStartVesting}th month`, () => {
          it('THEN the user has his corresponding tokens', async () => {
            const companyTokens = await inveCoin.vestingBalanceOf(companyAddress);
            const vestingDurationInMonths = new BN(companyMonthsToEndVesting).sub(
              new BN(companyMontshToStartVesting),
            );

            await advanceTime(monthToSeconds * (companyMontshToStartVesting + 1));
            await expect(await inveCoin.balanceOf(companyAddress)).to.be.bignumber.equal(new BN(0));
            await inveCoin.claim({ from: companyAddress });
            await expect(await inveCoin.balanceOf(companyAddress)).to.be.bignumber.equal(
              companyTokens.div(vestingDurationInMonths),
            );
          });
        });

        describe(`WHEN a company member tries to claim his tokens on the ${companyMonthsToEndVesting}th month`, () => {
          it(`THEN the user has ${companyTokensPercentage}% of the tokens`, async () => {
            const totalSupply = await inveCoin.totalSupply();
            await advanceTime(monthToSeconds * companyMonthsToEndVesting);
            await expect(await inveCoin.balanceOf(companyAddress)).to.be.bignumber.equal(new BN(0));
            const expectedTokens = totalSupply
              .mul(new BN(companyTokensPercentage))
              .div(new BN(100));
            await inveCoin.claim({ from: companyAddress });
            await expect(await inveCoin.balanceOf(companyAddress)).to.be.bignumber.equal(
              expectedTokens,
            );
          });
        });

        describe('WHEN a company member tries to claim his tokens twice', () => {
          it('THEN the second transaction fails', async () => {
            await advanceTime(monthToSeconds * (companyMontshToStartVesting + 1));
            await expect(await inveCoin.balanceOf(companyAddress)).to.be.bignumber.equal(new BN(0));
            await inveCoin.claim({ from: companyAddress });
            await inveCoin
              .claim({ from: companyAddress })
              .should.be.rejectedWith('The user has claimed all the tokens he can claim for now');
          });
        });
      });

      describe('AND no one has vested tokens charged as pre-sales', () => {
        let inveCrowdsale;
        let inveCoin;

        const openingTimeFromNow = 100;
        const crowdsaleDuration = 10000;
        const fundingCapInUsd = new BN('100000');
        const raisedPresaleInUsd = new BN('0');
        const companyAddress = thirdUser;

        const addressesToWhitelist = [owner, anUser, anotherUser];
        beforeEach(async () => {
          ({ inveCrowdsale, inveCoin } = await deployOpenCrowdsale(
            owner,
            openingTimeFromNow,
            crowdsaleDuration,
            fundingCapInUsd,
            raisedPresaleInUsd,
            addressesToWhitelist,
          ));
          await advanceTime(crowdsaleDuration + 1); // Make the crowdsale able to finish
          await inveCrowdsale.finish(companyAddress, { from: owner });
        });

        describe('WHEN any random person wants to claim tokens', () => {
          it('THEN cannot claim any token', async () => {
            await inveCoin
              .claim({ from: owner })
              .should.be.rejectedWith('User has not vesteable tokens');
            await inveCoin
              .claim({ from: owner })
              .should.be.rejectedWith('User has not vesteable tokens');
            await inveCoin
              .claim({ from: owner })
              .should.be.rejectedWith('User has not vesteable tokens');
          });
        });

        describe('WHEN any person from the company wants to claim tokens', () => {
          it('THEN he cannot claim any token', async () => {
            await inveCoin
              .claim({ from: companyAddress })
              .should.be.rejectedWith('User has not vesteable tokens');
          });
        });
      });

      describe('AND two users have vested tokens charged as pre-sales', () => {
        let inveCrowdsale;
        let inveCoin;

        const openingTimeFromNow = 100;
        const crowdsaleDuration = 10000;
        const fundingCapInUsd = new BN('100000');
        const raisedPresaleInUsd = new BN('0');
        const companyAddress = thirdUser;

        const addressesToWhitelist = [owner, anUser, anotherUser];
        beforeEach(async () => {
          ({ inveCrowdsale, inveCoin } = await deployOpenCrowdsale(
            owner,
            openingTimeFromNow,
            crowdsaleDuration,
            fundingCapInUsd,
            raisedPresaleInUsd,
            addressesToWhitelist,
          ));
          await inveCoin.addVestingBatch([anUser, anotherUser], [100, 100], [3, 3], [6, 6], {
            from: owner,
          });
          await advanceTime(crowdsaleDuration + 1); // Make the crowdsale able to finish
          await inveCrowdsale.finish(companyAddress, { from: owner });
        });
        describe('WHEN both claim their tokens separately', () => {
          it('THEN both transactions are successful', async () => {
            await advanceTime(monthToSeconds * 4);

            const { logs: logsFirstUser } = await inveCoin.claim({ from: anUser });
            expectEvent.inLogs(logsFirstUser, 'TokensClaimed', {
              amount: new BN(33),
              user: anUser,
            });
            expectEvent.inLogs(logsFirstUser, 'Transfer', {
              value: new BN(33),
              to: anUser,
              from: ZERO_ADDRESS,
            });

            const { logs: logsSecondUser } = await inveCoin.claim({ from: anotherUser });
            expectEvent.inLogs(logsSecondUser, 'TokensClaimed', {
              amount: new BN(33),
              user: anotherUser,
            });
            expectEvent.inLogs(logsSecondUser, 'Transfer', {
              value: new BN(33),
              to: anotherUser,
              from: ZERO_ADDRESS,
            });
          });
        });
      });

      describe('AND there are users that have pre-sale tokens and others that have private tokens', () => {
        let inveCrowdsale;
        let inveCoin;

        const openingTimeFromNow = 100;
        const crowdsaleDuration = 10000;
        const fundingCapInUsd = new BN('100000');
        const raisedPresaleInUsd = new BN('0');
        const companyAddress = thirdUser;

        const addressesToWhitelist = [owner, anUser, anotherUser];
        beforeEach(async () => {
          ({ inveCrowdsale, inveCoin } = await deployOpenCrowdsale(
            owner,
            openingTimeFromNow,
            crowdsaleDuration,
            fundingCapInUsd,
            raisedPresaleInUsd,
            addressesToWhitelist,
          ));
          // TODO Load tests that test at least 100 accounts

          await inveCoin.addVestingBatch([anUser], [100], [3], [6], { from: owner });
        });
        describe('WHEN the crowdsale finishes', () => {
          it(`THEN the company gets ${companyTokensPercentage}% of the final number of tokens`, async () => {
            await advanceTime(crowdsaleDuration + 1); // Make the crowdsale able to finish
            await inveCrowdsale.finish(companyAddress, { from: owner });
            const companyTokens = await inveCoin.vestingBalanceOf(companyAddress);
            const totalSupply = await inveCoin.totalSupply();

            await expect(
              totalSupply.mul(new BN(companyTokensPercentage)).div(new BN(100)),
            ).to.be.bignumber.equal(companyTokens);
          });
        });
        describe('AND someone buys tokens', () => {
          describe('WHEN the crowdsale finishes', () => {
            it(`THEN the company gets ${companyTokensPercentage}% of the final number of tokens`, async () => {
              const priceOfFirstTokenInWei = await inveCrowdsale.priceOfNextTokenInWei();
              await inveCrowdsale.buyTokens(owner, {
                from: owner,
                value: priceOfFirstTokenInWei * 100,
              });
              await advanceTime(crowdsaleDuration + 1); // Make the crowdsale able to finish
              await inveCrowdsale.finish(companyAddress, { from: owner });
              const companyTokens = await inveCoin.vestingBalanceOf(companyAddress);
              const totalSupply = await inveCoin.totalSupply();

              await expect(
                totalSupply.mul(new BN(companyTokensPercentage)).div(new BN(100)),
              ).to.be.bignumber.equal(companyTokens);
            });
          });
        });
      });
    });
  });
});
