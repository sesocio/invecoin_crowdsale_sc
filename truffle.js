const HDWalletProvider = require('truffle-hdwallet-provider')
const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  solc: {
    optimizer: {
      enabled: true,
      runs: 1,
    },
  },
  networks: {
    development: {
      host: '127.0.0.1',
      port: 8545,
      network_id: 5777,
      gas: 6721975,
      gasPrice: 20000000000,
    },
    dev: {
      host: '127.0.0.1',
      port: 8545,
      network_id: "*",
      gas: 6721975,
      gasPrice: 20000000000,
    },
    ropsten: {
      provider: new HDWalletProvider(
        process.env.WALLET_PKEY,
        'https://ropsten.infura.io/v3/<token>',
      ),
      network_id: 3,
      gas: 7600000,
    },
    rinkeby: {
      provider: new HDWalletProvider(
        process.env.WALLET_PKEY,
        'https://rinkeby.infura.io/v3/<token>',
      ),
      network_id: 4,
    },
    coverage: {
      host: 'localhost',
      network_id: '*', // eslint-disable-line camelcase
      port: 8555,
      gas: 0xfffffffffff,
      gasPrice: 0x01,
    },
    rskTestnet: {
      provider: new HDWalletProvider(process.env.WALLET_PKEY, 'https://public-node.testnet.rsk.co/'),
      network_id: '*',
      gasPrice: 65164000,
      gasLimit: 6800000
    },
    rskMain: {
      provider: new HDWalletProvider(process.env.WALLET_PKEY, 'https://public-node.rsk.co/'),
      network_id: '*',
      gasPrice: 65164000,
      gasLimit: 6800000
    },
  },
  mocha: {
    useColors: true,
    reporter: 'mochawesome',
  },
};
